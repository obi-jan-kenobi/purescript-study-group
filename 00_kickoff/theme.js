import { future } from 'mdx-deck/themes'

export default {
    ...future,
    font: 'Dank mono',
}
