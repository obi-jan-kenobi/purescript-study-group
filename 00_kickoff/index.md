# map (📝 -> ‍💡) 📖

# Ablauf heute
- Organisation
- Setup
- λ-Calculus

# Organisation
- Study-Group
    - Keine Vortragsreihe
    - Eigenverantworlich
    - Respektiert: Zeit, Commitment & Progress der Kollegen
- OS-Buch: PureScript by Example
    - Vom Entwickler der Sprache
    - Jede Woche 1 Kapitel in Eigenregie
- Sessions
    - Fundamentale Infos zusammenfassen
    - Aufgaben lösen
    - Speak Up
- Ziel: Grundlegendes Verstaendnis

# Getting started

- NodeJS & npm
```bash
npm i -g purescript@0.11.7
npm i -g pulp bower
```
- Alternative zu globaler Version / lokale Installation per Chapter
    - nicht wirklich noetig
- Buch basiert auf vergangener Compilerversion
- Gab breaking changes
- Kein Hindernis um Konzepte zu erarbeiten
## Resourcen
- `https://leanpub.com/purescript/read`
- `https://github.com/paf31/purescript-book`
- [JavaScript to PureScript](https://hackernoon.com/make-the-leap-from-javascript-to-purescript-5b35b1c06fef)
- [FP-Slack](https://functionalprogramming.slack.com/)
    - Invite noetig: [einfach diesen Bot fragen](https://fpchat-invite.herokuapp.com/)
    - #purescript & #purescript-beginners
    - Maintainer und Communitymembers
        - sehr hilfsbereit

## Editor
- IntelliJ 🤷‍
- Atom
- Vim
- VS Code 👍

##### `https://github.com/purescript/documentation/blob/master/guides/psc-ide-FAQ.md`

## VS Code
1. `https://code.visualstudio.com/`
2. Editor starten
3. `CTRL + P` > `ext install nwolverson.ide-purescript`
4. 🚀

## Chapter bearbeiten
- Ordner öffnen
- `bower install`
- `pulp --watch build`
    - um IDE die noetigen Infos zur Verfuegung zu stellen
    - Rebuild bei source change

## REPL
- `pulp repl`
    - `:browse <module>`
    - `import <module>`
    - `:reload`
    - `:type`
    - `:kind`
    - `:paste`
        - Multiline-Modus (Exit mit CTRL + D)
    - `:quit`
# Lambda-Calculus
- Entstanden 1933
- Äquivalent zu Turing Maschine
    - Anstatt ausfuehren von Instruktions Programmablauf durch Evaluierung von Expressions
- Fundament für _pure_ functional Languages
    - Referentielle Integrität
        - Expressions koennen durch ihre Werte ersetzt werden
    - Deterministisch
        - Gleicher Input fuehrt zu gleichem Output
    - Total
        - Jeder moegliche Input fuehrt zu einem Output
## Lambda Terms
- Expressions
- Values
- Functions
- Beispiel
    - λx.x
## Functions
- Head und Body
- Head gefolgt von Variable
- Body besteht aus Expression
- Head und Body durch . getrennt
- Beispiel
    - λx.x
    - λx := Head
    - x := Body

## Alpha Equivalenz
- Variablen haben keine Semantik
- λx.x = λz.z = λt.t

## Beta Reduction
- Entspricht Function Application
- Im Body ersetzen aller Vorkommnisse der Variable durch das Argument und elimieren des Heads
- Links Assoziativ
- Head mit mehreren Variablen sind eigentlich geschachtelte Lambda-Terme
    - λxy.yx = λxλy.yx = λx(λy.yx)
- Freie Variable = Taucht nur im Body auf
- Beispiel:
    - λxy.xy (λz.a) 1
    - λx(λy.xy) (λz.a) 1
    - [x:=λz.a]
    - λy.(λz.a)y 1
    - [y:=1]
    - λz.a 1
    - [z:=1]
    - a
## Beta Normal Form
- keine weitere Beta Reduction möglich
- Entspricht vollstaendig evaluierter Expression oder einem voll ausgefuehrten Program
- 2000/1000 -> zwar applied aber nicht evaluiert 
    - Normal Form = 2
## Combinators
- Lambda Term ohne freie Variable
## Divergenz
- Wenn Reduktion niemals terminiert
- Gegenteil von Convergenz (=> Beta Normal Form)
- Beispiel: (λx.xx)(λx.xx)
    - [x:(λx.xx)]
    - λx.xx(λx.xx)
## Fazit
- Funktionale Programmierung basiert auf Expressions
- Funktionen haben Head . Body
- Binden von Variablen findet in Deklaration statt
- Jede Funktion nimmt ein Argument entgegen! und gibt ein Ergebnis zurück
- Funktionen mappen von Input zu Output. Domain zu Range/Codomain
- Alles führt auf Lambda Calculus zurück
