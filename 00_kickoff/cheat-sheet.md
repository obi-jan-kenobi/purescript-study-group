# Lambda Calculus
## Struktur
- Expressions
- Values
- Functions
- λx.x
## Functions
- Head und Body
    - λx : Head
    - .x : Body
    - [x:=y] : Zuweisung
## Alpha Equivalenz
- Variablen haben keine Semantik
- λx.x = λz.z = λt.t
## Beta Reduction
- Bindet Variable an Argument
- Links assoziativ
- λxy.yx = λxλy.yx = λx(λy.yx)
## Beta Normal Form
- Keine weitere Reduction möglich
## Combinators
- Lambda Term ohne freie Variable
## Divergenz
- Wenn Reduktion niemals terminiert
