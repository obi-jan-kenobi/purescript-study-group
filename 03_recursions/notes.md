# Recursion
- haeufig in fp eingesetzt
- reduziert mutable state
- zielt auf divide & conquer ab
    - subprobleme loesen und dann loesungen zusammenfuegen

## Maps
- Aendert Inhalt erhaelt aber die Struktur
    - -> laeuft auf functors hinaus
- infix `<$>`

### Infix
- functions in backticks ` einschliessen
- infix functions sind aliase fuer existierende
- definiert durch `infix` `precedence` `function` `as` `operator`

### Precedence
- bestimmt welche expressions hoeher binden
- [Mehr Info](https://github.com/purescript/documentation/blob/master/language/Syntax.md#binary-operators)

## Do Notation
- bestehen aus:
    - Expressions die elemente eines arrays an einen namen binden
        - name links `<-` expression rechts
    - Expressions die nichts binden
    - Expressions die mit `let` expressions einen namen geben

### Guards
- haben dependency auf MondadZero
    - Fuer arrays ist zero gleichbedeutend mit []

### Folds
- bestehen aus accumulatorfunction startwert und foldable
- foldl und foldr unterscheiden sich in der richtung des traversal

### Tail Recursion
- der rekursive aufruf muss in einer expression als letztes geschehen
- anderes muss bereits evaluiert sein
- oftmals durch rekursion + accumulator moeglich
