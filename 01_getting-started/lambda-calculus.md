# Equivalenz

- \xy.xz
    1. \xz.xz
    2. `\mn.mz`
    3. \z.(\x.xz)


- \xy.xxy
    1. \mn.mnp
    2. \x.(\y.xy)
    3. `\a.(\b.aab)`

- \xyz.zx
    1. \x.(\y.(\z.z))
    2. `\tos.st`
    3. \mnp.mn

# Combinators

1. `\x.xxx`
2. \xy.zx
3. `\xyz.xy(zx)`
4. `\xyz.xz(zxy)`
5. \xy.xy(zxy)

# Divergenz

1. \x.xxx
    - Normal Form
2. (\z.zz)(\y.yy)
    - Diverge
3. (\x.xxx)z
    - -> `zzz` Normal Form

# Beta Reduction
1. (\abc.cba)zz(\wv.w)
    - [a:=z]
    - \bc.cbz(z)(\wv.w)
    - [b:=z]
    - \c.czz(\wv.w)
    - [c:=\wv.w]
    - \wv.w(z)z
    - [w:=z]
    - \v.z(z)
    - z
2. (\x.\y.xyy)(\a.a)b
    - [x:=\a.a]
    - \y.(\a.a)yy(b)
    - [y:=b]
    - (\a.a)bb
    - [a:=b]
    - bb
3. (\y.y)(\x.xx)(\z.zq)
    - [y:=\x.xx]
    - \x.xx(\z.zq)
    - [x:=\z.zq]
    - \z.zq(\z.zq)
    - [z:=\z.zq]
    - \z.zq(q)
    - qq
4. (\z.z)(\z.zz)(\z.zy)
    - (\z.z)(\x.xx)(\j.jy)
    - [z:=\x.xx]
    - (\x.xx)(\j.jy)
    - [x:=\j.jy]
    - \j.jy(\j.jy)
    - [j:=\j.jy]
    - \j.jy(y)
    - yy
5. (\x.\y.xyy)(\y.y)y
    - (\xy.xyy)(\z.z)y
    - [x:=\z.z]
    - \y.(\z.z)yy(y)
    - [y:=y]
    - \z.z(y)y
    - yy
6. (\a.aa)(\b.ba)c
    - [a:=\b.ba]
    - (\b.ba)(\b.ba)c
    - [b:=\b.ba]
    - \b.ba(a)c
    - [b:=a]
    - aac
7. (\xyz.xz(yz))(\x.z)(\x.a)
    - (\xyj.xj(yj))(\x.z)(\x.a)
    - [x:=\x.z]
    - (\yj.(\x.z)j(yj))(\x.a)
    - [y:=\x.a]
    - \j.(\x.z)j(\x.a)j
    - \j.z(\x.a)j
    - [x:=j]
    - \j.za

## True / False
- True := \xy.x
- False := \xy.y

### Not
- \x.x False True
    - [x:= False]
    - False False True
    - [False:=\xy.y]
    - (\xy.y) False True
    - [x:=False]
    - \y.y True
    - [y:=True]

### And
- \x\y.xy False
    - [x:=True]
    - \y.(\jk.j)y False
    - [y:=True]
    - (\jk.j)(\lm.m) False
    - [j:=\lm.m
    - \k.(\lm.m) False
    - [k:=False]
    - \lm.m => True

### Or
- \x\y.x True y
